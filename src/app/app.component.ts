import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  form: FormGroup;

  ngOnInit() {
    this.form = new FormGroup({
      'project-name': new FormControl(null, [Validators.required], this.asyncForbiddenProjectNames.bind(this)),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'project-status': new FormControl('Stable')
    });
  }

  forbiddenProjectNames(control: FormControl): {[s: string]: boolean} {
    if (control.value === 'Test') {
      return {'projectNameIsForbidden': true};
    }
    return null;
  }

  asyncForbiddenProjectNames(control: FormControl): Promise<any> | Observable<any> {
    const promise = new Promise<any>(((resolve, reject) => {
      setTimeout(() => {
        if (control.value === 'Test') {
          resolve({'projectNameIsForbidden': true});
        } else {
          resolve(null);
        }
      }, 1500);
    }));
    return promise;
  }

  onSubmit() {
    console.log('form values', this.form.value);
  }

}
